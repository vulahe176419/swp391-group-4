USE [master]
GO
/****** Object:  Database [job_searching_project]    Script Date: 28/03/2024 12:26:20 ******/
CREATE DATABASE [job_searching_project]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'job_searching_project', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\job_searching_project.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'job_searching_project_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\job_searching_project_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [job_searching_project] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [job_searching_project].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [job_searching_project] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [job_searching_project] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [job_searching_project] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [job_searching_project] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [job_searching_project] SET ARITHABORT OFF 
GO
ALTER DATABASE [job_searching_project] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [job_searching_project] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [job_searching_project] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [job_searching_project] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [job_searching_project] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [job_searching_project] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [job_searching_project] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [job_searching_project] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [job_searching_project] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [job_searching_project] SET  ENABLE_BROKER 
GO
ALTER DATABASE [job_searching_project] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [job_searching_project] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [job_searching_project] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [job_searching_project] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [job_searching_project] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [job_searching_project] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [job_searching_project] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [job_searching_project] SET RECOVERY FULL 
GO
ALTER DATABASE [job_searching_project] SET  MULTI_USER 
GO
ALTER DATABASE [job_searching_project] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [job_searching_project] SET DB_CHAINING OFF 
GO
ALTER DATABASE [job_searching_project] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [job_searching_project] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [job_searching_project] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [job_searching_project] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'job_searching_project', N'ON'
GO
ALTER DATABASE [job_searching_project] SET QUERY_STORE = ON
GO
ALTER DATABASE [job_searching_project] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [job_searching_project]
GO
/****** Object:  Table [dbo].[account]    Script Date: 28/03/2024 12:26:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[account](
	[account_id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [varchar](45) NULL,
	[email] [varchar](45) NULL,
	[password] [varchar](45) NULL,
	[phone] [varchar](45) NULL,
	[address] [varchar](45) NULL,
	[specialism] [varchar](45) NULL,
	[status] [varchar](45) NULL,
	[avatar] [varchar](500) NULL,
	[role_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[account_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[apply]    Script Date: 28/03/2024 12:26:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[apply](
	[apply_id] [int] IDENTITY(1,1) NOT NULL,
	[account_id] [int] NULL,
	[job_id] [int] NULL,
	[resume_id] [int] NULL,
	[submission_date] [varchar](45) NULL,
	[status] [varchar](45) NULL,
PRIMARY KEY CLUSTERED 
(
	[apply_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[category]    Script Date: 28/03/2024 12:26:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[category_id] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [varchar](45) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job]    Script Date: 28/03/2024 12:26:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job](
	[job_id] [int] IDENTITY(1,1) NOT NULL,
	[job_name] [varchar](45) NULL,
	[description] [varchar](max) NULL,
	[requirement] [varchar](max) NULL,
	[benefit] [varchar](max) NULL,
	[contact_mail] [varchar](45) NULL,
	[offer_salary] [varchar](45) NULL,
	[level] [varchar](45) NULL,
	[experience] [varchar](max) NULL,
	[qualification] [varchar](max) NULL,
	[category_id] [int] NULL,
	[type] [varchar](max) NULL,
	[location] [varchar](45) NULL,
	[posted_date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[job_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[resumes]    Script Date: 28/03/2024 12:26:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[resumes](
	[resume_id] [int] IDENTITY(1,1) NOT NULL,
	[resume_name] [varchar](500) NULL,
	[account_id] [int] NULL,
	[full_name] [varchar](500) NULL,
	[phone] [varchar](500) NULL,
	[address] [varchar](500) NULL,
	[education] [varchar](500) NULL,
	[experience] [varchar](500) NULL,
	[portfolio_url] [varchar](500) NULL,
	[skill] [varchar](500) NULL,
	[award] [varchar](500) NULL,
 CONSTRAINT [PK_resumes] PRIMARY KEY CLUSTERED 
(
	[resume_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[role]    Script Date: 28/03/2024 12:26:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[role_name] [varchar](45) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wishlist]    Script Date: 28/03/2024 12:26:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wishlist](
	[wishlist_id] [int] IDENTITY(1,1) NOT NULL,
	[account_id] [int] NOT NULL,
	[job_id] [int] NOT NULL,
 CONSTRAINT [PK_wishlist] PRIMARY KEY CLUSTERED 
(
	[wishlist_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[account] ON 

INSERT [dbo].[account] ([account_id], [user_name], [email], [password], [phone], [address], [specialism], [status], [avatar], [role_id]) VALUES (1, N'admin', N'admin@gmail.com', N'123', N'0912123456', N'Ha Noi', N'none', N'1', N'none', 2)
SET IDENTITY_INSERT [dbo].[account] OFF
GO
SET IDENTITY_INSERT [dbo].[category] ON 

INSERT [dbo].[category] ([category_id], [category_name]) VALUES (1, N'Marketing')
INSERT [dbo].[category] ([category_id], [category_name]) VALUES (2, N'Graphic Design')
INSERT [dbo].[category] ([category_id], [category_name]) VALUES (3, N'Information Technology')
INSERT [dbo].[category] ([category_id], [category_name]) VALUES (4, N'Finance')
SET IDENTITY_INSERT [dbo].[category] OFF
GO
SET IDENTITY_INSERT [dbo].[job] ON 

INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (2, N'Senior Software Engineer', N'Partner with the Product Manager and team to create and improve new features on top of GPT technology.  Conduct extensive technical analysis and design to break down the feature into high-level design and business requirements.  Develop and review code that adheres to development standards for code style, design patterns, readability, and maintainability, as well as incorporating scaling best practices.  Collaborate with engineers to increase the systems technical responsibility in order to ensure release product quality, security, and performance.  Use Code Pilot to improve development efficiency and quality by automating repetitive operations and spotting any flaws or issues early on.  Identify potential points of improvement within the current codebase and propose strategies to address them.', N'Bachelors degree in Computer Science, Engineering, or a related field;  Solid experience with JVM-based languages (Java) and Python.  Strong background in conventional J2EE specifications, experience with back-end web frameworks (Spring), ORMs (Hibernate), testing (JUnit, Mockito), etc.  Experience with SDLC and popular tools like JIRA, SonarQube, Jenkins, Selenium, etc.  Experience in using Generative AI technology and its techniques in software systems is an advantage.  Hands-on experience with Terraform, and OpenTelemetry is preferable.  Fluent English communication skills.', N'Attractive salary with 13-month bonuses and periodic performance  19 annual leave days, supportive allowances, and public holidays  Flexible working hours', N'donnylee2003@gmail.com', N'$800', N'Senior', N'1+ years of experience', N'Experienced (non-manager)', 3, N'Full Time', N'Ho Chi Minh', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (6, N'Merchant Marketing Executive', N'Develop and execute marketing and branding strategies to drive brand awareness and customer acquisition across multiple channels including digital, social, events, and other mediums. Develop and maintain brand guidelines and ensure consistency in messaging and visual identity across all customer touchpoints. Conduct market research and analyze customer data to inform marketing and branding strategies and identify new opportunities for growth. Work with cross-functional teams to develop and execute campaigns that drive business objectives, also ensure brand consistency across all product offerings. Develop and maintain relationships with key industry partners and affiliates.', N'Bachelor''s degree in Marketing, Business Administration, or a related field. A Master''s degree is preferred. Experience in the fintech or financial services industry is an advantage. Proven experience in developing and executing successful marketing and branding campaigns across multiple channels. Strong understanding of marketing strategy development, customer segmentation, and consumer behavior. Experience with market research and data analysis. Excellent project management skills and ability to manage multiple projects simultaneously. Strong communication and interpersonal skills.', N'Competitive compensation package, including 13th-month salary and performance bonuses Comprehensive health care coverage for you and your dependents Generous leave policies, including annual leave, sick leave, and flexible work hours Convenient central district 1 office location, next to a future metro station Onsite lunch with multiple options, including vegetarian Grab for work allowance and fully equipped workstations Fun and engaging team building activities, sponsored sports clubs, and happy hour every Thursday Unlimited free coffee, tea, snacks, and fruit to keep you energized', N'donnylee2003@gmail.com', N'$2000', N'Manager', N'7+ years of experience in Corporate marketing, branding, brand management, or related fields.', N'Manager', 1, N'Full Time', N'Ho Chi Minh', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (7, N'Graphic Design Intern', N'Assist in the design and development of visual materials for marketing campaigns, including service portfolio documents, presentations, social media graphics, brochures, posters, email templates, website content (using Wordpress), etc. Support the design team in conceptualizing and implementing creative ideas for brand initiatives, ensuring consistency with SmartDev''s brand identity. Participate in brainstorming sessions and contribute fresh ideas for project designs and campaigns. Collaborate with the marketing team to understand the design requirements and deliver compelling visual content that resonates with our target audience. Assist in the maintenance and organization of digital assets, including images, logos, branding guidelines and design files. Stay abreast of the latest design trends and technologies to bring innovative ideas to the team.', N'We understand the demands of studying, and we''re committed to accommodating your schedule. You''ll be required to join us at least 3 times/week at the office and then you can be flexible for the rest of time.', N'Benefit from the guidance and expertise of Senior Marketing Executives & Manager with accumulated 20 years of experience in B2B and IT Fields.', N'donnylee2003@gmail.com', N'$100', N'Intern', N'0.5 - 1 years of experience', N'Currently pursuing a degree in Graphic Design, Visual Arts, Marketing, Communications, or a related field. Proficiency in Adobe Creative Suite (Photoshop, Illustrator, InDesign, Premier, Lightroom) and other graphic design softwares or online platforms (Canva, Figma, Sketch, draw.io, etc.). Video editing skill is preferable. A strong portfolio that demonstrates creativity, graphic design skills, and an understanding of branding principles.', 2, N'Part Time', N'Ha Noi', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (8, N'Frontend Developer', N'The successful candidate will play a pivotal role in front-end codebase design and development, leading and managing projects from a technical perspective, and collaborating closely with UX designers, designers, and clients to create impressive websites. This role requires a strong foundation in TypeScript/JavaScript, CSS (SCSS/PostCSS), and a solid understanding of cloud infrastructure. If you are a creative problem-solver with a passion for emerging technologies, this opportunity is for you.', N'Design and develop the front-end codebase. Lead and manage technical aspects of projects. Evaluate and select appropriate technologies and libraries for each project. Collaborate with UX and graphic designers to bridge design and technical implementation. Work closely with clients to define the overall look and functionality of websites. Stay updated with industry trends and emerging technologies. Collaborate with international team members.', N'1 day work from home per week 13th-month salary and summer bonus (subject to the company''s discretion). Biannual pay raise reviews. 14 annual leave days in compliance with Vietnamese government regulations, along with 1 special day. 100% salary during probation Full salary coverage for compulsory insurances', N'donnylee2003@gmail.com', N'$800', N'Junior ', N'1+ years of experience', N'Proven experience in developing front-end projects from the ground up. Proficiency in TypeScript/JavaScript, CSS (SCSS/PostCSS), ReactJS Strong command of the English language (must)', 3, N'Full Time', N'Ha Noi', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (9, N'Graphic Designer', N'We are seeking a Junior Graphic Designer to join our Organizational Communications Team. The ideal candidate should have a strong understanding of current design trends and possess the ability to create visually impactful solutions. As a Junior Graphic Designer, you will be responsible for designing a variety of key visuals for events, our fan page, and overseeing the art direction for image-related activities. This includes creating branded items, slide decks, and various products such as event point-of-sale materials (POSM), leaflets, brochures, digital ads, UI design, and infographics.', N'Develop designs, concepts, and sample layouts based on knowledge of design principles and aesthetic concepts. Execute the creative process for communication materials, including new product launches, key visuals, promotional items, marketing materials, and website designs, to attract the attention of the target audience towards our products and services. Handle graphic design tasks such as social media posts, online ads, websites, blogs, etc., and stay up-to-date with current social and marketing campaigns. Gather information and materials to prepare work for accomplishment. Illustrate concepts by designing rough layouts of art and copy, considering arrangement, size, type size and style, and related aesthetic concepts. Collaborate with teammates or supervisors to discuss concepts and make necessary changes. Ensure final graphics and layouts are visually appealing and aligned with the brand.', N'Two performance appraisals every year and performance bonus Onsite opportunities: short-term and long-term assignments in North American (U.S, Canada), Europe, Asia. Flexible working time Various training on hot-trend technologies, best practices and soft skills Premium healthcare insurance for you and your loved ones', N'donnylee2003@gmail.com', N'$1800', N'Designer', N'2+ years of graphic design experience', N'Designer', 2, N'Full Time', N'Ho Chi Minh', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (10, N'Finance Accounting Manager', N'Handle full set of bookkeeping, overseeing accounts payable, accounts receivable, payroll, and bank reconciliations. Prepare accurate monthly, quarterly, and annual financial reports. Ensure adherence to accounting principles and local financial regulations. Coordinate with external auditors for annual audits.', N'Provide comprehensive administrative assistance to the investment team. Organize and maintain files and records, ensuring confidentiality and accuracy. Schedule and manage meetings, appointments, and travel arrangements. Prepare and revise correspondence, reports, and presentations. Manage communication with stakeholders, handling emails and phone calls.', N'Exposure to exciting technology investments within the investment sphere. Close collaboration with skilled investment professionals and top-tier external counsels. Engaging and intellectually stimulating assignments. Remote work with occasional in-person meetings. Flexible work arrangements and a highly supportive work environment.', N'donnylee2003@gmail.com', N'$2350', N'Manager', N'5+ years experience  in bookkeeping and administrative roles, preferably within a family office or investment firm.', N'Bachelors degree in Finance, Accounting, Business Administration, or a related field.', 4, N'Full Time', N'Ha Noi', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (11, N'Deputy Manager Finance', N'Supervise and oversee all transaction processing activities handled by the Accounts Payable, Accounts Receivable, and Operational Accounting teams, including retail transactions.  Ensure the timely closure of month-end processes and the accurate generation of reports based on agreed Service Level Agreements (SLAs), addressing any inquiries from the Country Finance Teams regarding processed data.  Ensure adherence to appropriate governance structures, policies, and procedures, while consistently seeking opportunities for enhancement.', N'Develop reports on finance efficiency metrics (such as DSO, DPO, invoices per FTE, days to close), provide insights, and generate Key Performance Indicator (KPI) reports to drive continuous improvement.  Manage team scheduling, conduct workforce planning, recruit and train staff on updated processes and systems, and oversee performance management.  Offer mentorship and coaching to the team members, fostering their professional growth and development.', N'Dynamic and inclusive international work culture that integrates the strengths of diverse backgrounds, fostering an energetic, commercial, and enjoyable work environment.  Opportunity to pioneer a new and innovative Flywheel product supported by robust technology, holding a significant role within the Flywheel Insights team addressing complex data challenges.', N'donnylee2003@gmail.com', N'$2800', N'Manager', N'3+ years of relevant work experience, with preference for experience in a shared service center environment.', N'Manager', 4, N'Full Time', N'Ho Chi Minh', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (12, N'Marketing Content Writer', N'You will be responsible for conducting an in-depth analysis of Ideal Customer Profiles, potential clients, industries, and market trends to help guide the overall marketing strategy. Additionally, you will drive lead generation, brand awareness, and help educate our target audience by creating unique marketing collateral. The ideal candidate will have strong English proficiency, possess a creative edge and attention to detail, have the ability to think outside the box, and have a keen analytical mind.', N'Conduct comprehensive research to define and refine Ideal Customer Profiles (ICPs) by analyzing demographics, behaviors, and market needs. Work collaboratively with cross-functional teams to tailor messaging and ensure alignment between go-to-market strategies and overall business objectives. Identify and research potential clients by gathering information on companies, revenue, decision-makers, and key stakeholders. Support the sales team in targeted outreach and engagement strategies. Conduct continuous research on industry trends, market dynamics, and competitive landscapes. Segment target markets and mailing lists based on research findings, ensuring personalized and effective marketing approaches. Create and manage a blog calendar of a variety of relevant industry topics. Leverage Generative AI tools like ChatGPT to create detailed blog outlines for our partner agency, resulting in impacts on SEO and website optimization. Conduct research and create content outline plans infographics, checklists, datasheets. Write multi-page ebooks and whitepapers for specific audiences on trending topics or certain KMS service areas.', N'Building large-scale & global software products Working & growing with Passionate & Talented Team Diverse careers opportunities with Software Outsourcing, Software Product Development, IT Solutions & Consulting Attractive Salary and Benefits Two performance appraisals every year and performance bonus Onsite opportunities: short-term and long-term assignments in North American (U.S, Canada), Europe, Asia. Flexible working time', N'donnylee2003@gmail.com', N'$750', N'Content Writer', N'2+ years of experience in Marketing or similar roles, specifically content creation.', N'Content Writer', 1, N'Part Time', N'Ha Noi', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (13, N'Performance Marketing Manager/Expert', N'Our client is a leading delivery platform-based company that is looking for talent to join their firm:  To develop digital marketing plan to help companys clients to raise their online sales revenue.  Set marketing objectives and managing all marketing and advertising initiatives in line with brand goals.  Recognize market prospects and encourage the expansion of business outcomes to get a deeper understanding of the market and consumer preferences in the logistics sector.  Develop, improve, guarantee adherence to frameworks, procedures, and policies linked to specific clients  Establishing ambitious Objectives and Key Results (OKRs) can ensure constant strategic and tactical alignment across your team.  Create, motivate, and manage a high-performing marketing team that achieves marketing objectives via the use of roles, career pathways, and employee success initiatives as well as KPIs and performance measurement that are clearly defined.', N'Proven work experience in management roles.  Prior experience in Logistics/Ecommerce-related environment  Mastery of product marketing at the expert level (messaging, product launches, strategy, etc.).  Strong familiarity with all indicators used to assess the success of marketing use measurements and statistics to inform decisions.  Tactics for gaining experience using various marketing channels  Marketer with expertise expanding both digital and non-digital channels  Excellent English communication abilities, both vocal and written.', N'Competitive compensation package, including 13th-month salary and performance bonuses Comprehensive health care coverage for you and your dependents Generous leave policies, including annual leave, sick leave, and flexible work hours Convenient central district 1 office location, next to a future metro station Onsite lunch with multiple options, including vegetarian Grab for work allowance and fully equipped workstations Fun and engaging team building activities, sponsored sports clubs, and happy hour every Thursday Unlimited free coffee, tea, snacks, and fruit to keep you energized', N'donnylee2003@gmail.com', N'$4000', N'Manager/Expert', N'8+ years of B2C and/or B2B marketing experience', N'Manager/Expert', 1, N'Full Time', N'Ha Noi', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (14, N'Software Engineer', N'Develop software applications for various operating systems such as Linux, Windows, or MacOS. Build professional UI with support from UI/UX design team. Construct software processes to communicate with peripheral devices (camera, circuit board, GPU...), databases (MySQL, SQLite...), and cloud servers. Integrate modules for artificial intelligence, image processing, camera resource management, and GPU resource management. Program across multiple operating systems and create projects with clear architecture. Program robot controls in automated systems.', N'Proficiency in C/C++ programming on Linux. Ability to read, understand, and write reports in English. Strong teamwork skills. Ability to handle high-pressure work environments. Preferred qualifications include: Experience using Microsoft Visual Studio, Qt framework, Visual Studio code. Experience working with mobile devices, embedded systems, or developing applications for these devices. Experience in image processing application programming is a plus.', N'Salary reviews twice a year in April and October. 13th-month salary and project bonuses at the end of the year. Opportunities for promotion to Project Manager or Leader based on demonstrated managerial skills. Innovation bonuses for projects. Quarterly awards for outstanding employees. Training opportunities to participate in real projects deploying automation machinery for major US clients. Health insurance coverage for employees and three family members. Participation in annual team building, travel, and outdoor activities. Free lunch, afternoon snack, milk, fruits, coffee, and pastries. Opportunity to work with colleagues and clients from various countries.', N'donnylee2003@gmail.com', N'$2100', N'Engineer', N'1-2 years of experience working as a Software Engineer programming in C/C++ on Linux.', N'Engineer', 3, N'Full Time', N'Ho Chi Minh', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (15, N'Information Technology Administrator', N'Manage annual IT equipment and system planning and execution. Uphold group IT guidelines and requirements in all IT processes. Provide technical support to both office and field IT users. Partner with various departments to propose and implement initiatives that enhance work efficiency and security. Conduct regular IT assessments and audits. Implement mandatory IT security protocols as per group instructions. Continuously educate and monitor users regarding IT security compliance. Develop and implement automation solutions and local initiatives to streamline operations. Champion safety within the team by promoting safe work practices and reporting any unsafe events. Ensure adherence to the company''s Code of Conduct and all local laws. Report any observed violations or potential violations to management.', N'Strong understanding of network monitoring and security practices. Experience with industry-standard technologies like VMware, Cisco, Dell, Symantec, and Microsoft. Ability to work independently and manage pressure effectively. Excellent communication skills in both English and Vietnamese. Passion for staying current with advancements in cloud, SDWAN, and information security. Strong customer service orientation and a willingness to develop clear instructions and user guides.', N'Competitive compensation package, including 13th-month salary and performance bonuses Comprehensive health care coverage for you and your dependents Generous leave policies, including annual leave, sick leave, and flexible work hours Convenient central district 1 office location, next to a future metro station Onsite lunch with multiple options, including vegetarian Grab for work allowance and fully equipped workstations Fun and engaging team building activities, sponsored sports clubs, and happy hour every Thursday Unlimited free coffee, tea, snacks, and fruit to keep you energized', N'donnylee2003@gmail.com', N'$2300', N'Administrator', N'3+ years of experience in IT network and system administration.', N'Administrator', 3, N'Full Time', N'Ha Noi', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (16, N'Unity Games Developer', N'Develop new features for our client''s application. Maintenance and optimize existing features. Brainstorm to make game ideas, and find out solutions for new features. Support other members about technology and solutions. Communicate and report directly to Leaders in Vietnam and Canada.', N'Strong knowledge of C# and OOP. Strong knowledge of Unity3D development. Have knowledge of Android development. Have knowledge of creating Android plugins for Unity3D. Have knowledge of multiplayer. Have knowledge of optimizing games. Proficient in English (Reading and Writing).', N'Performance appraisal every year; Attractive bonus every year; Health Care Program upto 2000$/year; Breakfast allowance; Happy Friday; Opportunities to train/work in Canada; International, open, and respectful working environment Sports activities; Yearly company trip; Team building activities; Technical seminar in the company;', N'donnylee2003@gmail.com', N'$1400', N'Developer', N'2+ years of experience in a similar position.', N'Developer', 3, N'Full Time', N'Ho Chi Minh', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (17, N'Senior Front-end Software Engineer (ReactJS)', N'Love Coding. Follow Your Passion. Enjoy Every Single Working Day with Your Nice Colleagues and Our Kind Clients. Take Your Skill to the Next Level. The responsibility includes: design, coding, troubleshooting and working with project team as well as customers on features/bug fixes.', N'Expert in HTML5, CSS3 and JavaScript/ES6 Strong experience in ReactJS Experience in building a single page application including front-end routing, 2-way data binding, client-side rendering, and JSON-based RESTful APIs Experience with Responsive Layouts Experience in the Unit Test frameworks such as Karma/Mocha, Jasmine, QUnit Practice knowledge of website optimization techniques and fast page load times Passionate about building a great UI/UX apps Additional Information', N'Working in one of the Best Places to Work in Vietnam Building large-scale & global software products Working & growing with Passionate & Talented Team Diverse careers opportunities with Software Services, Software Product Development, IT Solutions & Consulting Attractive Salary and Benefits Two performance appraisals every year and performance bonus Onsite opportunities: short-term and long-term assignments in North American (U.S, Canada), Europe, Asia. Flexible working time Various training on hot-trend technologies, best practices and soft skills Premium healthcare insurance for you and your loved ones Company trip, big annual year-end party every year, team building, etc. Fitness & sport activities: football, tennis, table-tennis, badminton, yoga, swimming… Joining community development activities: 1% Pledge, charity every quarter, blood donation, public seminars, career orientation talks,…', N'donnylee2003@gmail.com', N'$2220', N'Engineer', N'5+ years of experience ', N'Engineer', 3, N'Full Time', N'Ha Noi', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (18, N'Medical Marketing Doctor', N'Seeking a part-time Medical Marketing Doctor to play a key role in reviewing and writing medical content for consumer and doctor website interfaces, with a focus on result explanations and trade marketing. In addition, this role will involve gathering and structuring disease treatment protocols to standardize treatment prescribing and routing protocols. Our goal is to ultimately support our patients and referral doctors with better data as well as reporting and decision making insights. The ideal candidate will work closely with the Diag digital marketing team, lab doctors, Supervisor Doctor, and Branch Operations Coordinator to ensure the accuracy and effectiveness of medical content and protocols.Some of the work you will do will be basic – creating and revising test and disease descriptions and explanations. ', N'1. Prefer Female, under 29 years2. Medical degree from a top University and ideally a high school for the gifted3. Knowledge of medical terminology, disease treatment protocols4. Good English language skills with an interest on improving via practice and communications5. Strong writing and editing skills, interest in communicating complex medical information in a clear and concise manner, including to non-medical users6. Strong interest in learning about marketing and consumer diagnostics7. Ability to work collaboratively with cross-functional teams, including digital marketing, lab doctors, and telemedicine professionals8. Proficiency in Microsoft Office and willingness to work in content management systems9. Excellent organizational and time management skills, with the ability to prioritize tasks effectively', N'Competitive compensation package, including 13th-month salary and performance bonuses Comprehensive health care coverage for you and your dependents Generous leave policies, including annual leave, sick leave, and flexible work hours Convenient central district 1 office location, next to a future metro station Onsite lunch with multiple options, including vegetarian Grab for work allowance and fully equipped workstations Fun and engaging team building activities, sponsored sports clubs, and happy hour every Thursday Unlimited free coffee, tea, snacks, and fruit to keep you energized', N'donnylee2003@gmail.com', N'$600', N'Part-Time', N'1+ years of experience', N'Part-Time', 1, N'Part Time', N'Ho Chi Minh', CAST(N'2024-03-13' AS Date))
INSERT [dbo].[job] ([job_id], [job_name], [description], [requirement], [benefit], [contact_mail], [offer_salary], [level], [experience], [qualification], [category_id], [type], [location], [posted_date]) VALUES (19, N'3D Game Developer', N'We are looking for talented Unity developers to join our growing team and work on exciting projects. As part of our team, you will be involved in both existing and new 3D game development. Your responsibilities will include:  Developing and maintaining cross-platform games using Unity. Collaborating closely with the product owner and creative team to implement tasks based on project documents. Ensuring timely delivery of work, meeting required standards, and adhering to delivery process constraints.', N'Proficient in C# programming with a strong understanding of Object-Oriented Programming (OOP) principles. Experience in game or software development using the Unity Engine, including scripting, textures, animation, and GUI styles, is advantageous. Knowledge of performance profiling and optimizing Unity applications for various devices is a plus. Familiarity with coding, software architecture, and 3D math concepts is beneficial. Experience with 3D assets and basic proficiency in Blender or similar 3D graphics software is advantageous. Why You''ll Love Working Here:If you resonate with words like "fun," "challenging," "friendly," "supportive," "collaborative," and "innovative," and if you''re looking for a workplace that feels like home, then we invite you to join us.', N'Supportive and friendly work environment. Annual company trips. Free English classes to support language learning. Company-provided health insurance. Various company activities such as birthday parties and football games. Well-stocked kitchen with snacks and fruits for employees. Opportunities for career growth. Competitive salary with a 13th-month bonus. Performance-based salary reviews conducted twice per year.', N'donnylee2003@gmail.com', N'$1900', N'Developer', N'2+ years of experience', N'Developer', 3, N'Full Time', N'Ha Noi', CAST(N'2024-03-13' AS Date))
SET IDENTITY_INSERT [dbo].[job] OFF
GO
SET IDENTITY_INSERT [dbo].[role] ON 

INSERT [dbo].[role] ([role_id], [role_name]) VALUES (1, N'user')
INSERT [dbo].[role] ([role_id], [role_name]) VALUES (2, N'admin')
SET IDENTITY_INSERT [dbo].[role] OFF
GO
ALTER TABLE [dbo].[account]  WITH CHECK ADD  CONSTRAINT [FK_role] FOREIGN KEY([role_id])
REFERENCES [dbo].[role] ([role_id])
GO
ALTER TABLE [dbo].[account] CHECK CONSTRAINT [FK_role]
GO
ALTER TABLE [dbo].[apply]  WITH CHECK ADD  CONSTRAINT [FK_account_apply] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[apply] CHECK CONSTRAINT [FK_account_apply]
GO
ALTER TABLE [dbo].[apply]  WITH CHECK ADD  CONSTRAINT [FK_apply_resume] FOREIGN KEY([resume_id])
REFERENCES [dbo].[resumes] ([resume_id])
GO
ALTER TABLE [dbo].[apply] CHECK CONSTRAINT [FK_apply_resume]
GO
ALTER TABLE [dbo].[apply]  WITH CHECK ADD  CONSTRAINT [FK_job_apply] FOREIGN KEY([job_id])
REFERENCES [dbo].[job] ([job_id])
GO
ALTER TABLE [dbo].[apply] CHECK CONSTRAINT [FK_job_apply]
GO
ALTER TABLE [dbo].[job]  WITH CHECK ADD  CONSTRAINT [FK_category_job] FOREIGN KEY([category_id])
REFERENCES [dbo].[category] ([category_id])
GO
ALTER TABLE [dbo].[job] CHECK CONSTRAINT [FK_category_job]
GO
ALTER TABLE [dbo].[resumes]  WITH CHECK ADD  CONSTRAINT [FK_resumes_account] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[resumes] CHECK CONSTRAINT [FK_resumes_account]
GO
ALTER TABLE [dbo].[wishlist]  WITH CHECK ADD  CONSTRAINT [FK_wishlist_account] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
ALTER TABLE [dbo].[wishlist] CHECK CONSTRAINT [FK_wishlist_account]
GO
ALTER TABLE [dbo].[wishlist]  WITH CHECK ADD  CONSTRAINT [FK_wishlist_job] FOREIGN KEY([job_id])
REFERENCES [dbo].[job] ([job_id])
GO
ALTER TABLE [dbo].[wishlist] CHECK CONSTRAINT [FK_wishlist_job]
GO
USE [master]
GO
ALTER DATABASE [job_searching_project] SET  READ_WRITE 
GO
